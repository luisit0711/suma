


import 'package:flutter/material.dart';

class HomePage extends StatelessWidget{

final contador=10;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Prueba Titulo'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.more_vert,
            color: Colors.white,
            ),
             onPressed: (){})],
             centerTitle: true,
      ),
    
      body: Center(
        child: Column(
          children: <Widget>[
            Text('Numero de Clicks',style: TextStyle(fontSize: 25.0),),
            Text('$contador', style: TextStyle(fontSize: 30.0),)
          ],
          mainAxisAlignment: MainAxisAlignment.center,
          ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){},
        child: Icon(Icons.add_box),

      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}